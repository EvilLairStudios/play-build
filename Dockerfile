FROM 	docker:stable-dind

RUN 	apk --no-cache add openjdk8 bash && \
	mkdir /opt && cd /opt &&\
	wget https://github.com/sbt/sbt/releases/download/v1.1.1/sbt-1.1.1.tgz &&\
	tar -xzvf sbt-1.1.1.tgz

ENV	PATH=${PATH}:/opt/sbt/bin

#	play_$PLAY-VERSION_$SCALA-VERSION_$SBT-VERSION
COPY	play_2.6.12-2.12.4-1.1.1/ /tmp/play_2.6.12-2.12.4-1.1.1

# Run tests and builds to pre-download most of our base jars
RUN	cd /tmp/play_2.6.12-2.12.4-1.1.1 &&\
	sbt test &&\
	sbt docker:stage &&\
	cd / &&\
	rm -rf /tmp/play_2.6.12-2.12.4-1.1.1

